﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class EventStateBoolHandler : MonoBehaviour
{
    private TextMeshProUGUI tmpText = null;

    // Start is called before the first frame update
    void Awake()
    {
        tmpText = gameObject.GetComponent<TextMeshProUGUI>();
    }

    public void Invoked(string state, bool value, string name)
    {
        Debug.Log("Invoked(" + state + ", " + value + ", " + name + ")", this);
        // Run state ? 
        if (tmpText != null)
        {
            tmpText.text = name;
        }
    }
}
