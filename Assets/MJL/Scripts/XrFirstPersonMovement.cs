﻿// Unity XR First Person - Movement Script
//
// Movement:     https://www.youtube.com/playlist?list=PLmc6GPFDyfw87eECUxsoysoIz82VifRwt
// Gravity/Jump: https://answers.unity.com/questions/334708/gravity-with-character-controller.html
//
// ==============================================================================
//
// MIT License
//
// Copyright(c) 2020 Martin Looker
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public partial class XrFirstPerson : LocomotionProvider
{
    [Header("XR Controllers")]
    public XRController controllerLeft;
    public XRController controllerRight;
    [Header("Options")]
    public bool canJump   = true;
    public bool canRun    = true;
    public bool canCrouch = true;
    public bool canFly    = true;
    [Header("Events")]
    public EventStateBool eventStateRun;
    public EventStateBool eventStateCrouch;
    public EventStateBool eventStateFly;

    // Objects
    private CharacterController characterController = null;
    private GameObject objectPlayer;
    private GameObject objectXrRig;
    private GameObject objectCameraFloorOffset;
    private GameObject objectMainCamera = null;
    // Movement
    private float   speedWalk = 3.0f;
    private float   speedRun  = 6.0f;
    private float   speedJump = 6.0f;
    private Vector3 vectorMove;
    private float   speedY;
    // Rotation
    private int   snapState     = 0;
    private float snapTimer     = 0.0f;
    private float snapThreshold = 0.7f;
    private float snapTimeout   = 0.1f;
    private float snapDegrees   = 45.0f;    
    private float rotateY;
    // Height
    private float heightStand  = 1.75f;
    private float heightCrouch = 1.0f;
    // States
    private bool stateRun    = false;
    private bool stateCrouch = false;
    private bool stateFly    = false;
    // Controls
    private XrInputBool    xrInputBoolJump;
    private XrInputBool    xrInputBoolRun;
    private XrInputBool    xrInputBoolCrouch;
    private XrInputBool    xrInputBoolFly;
    private XrInputVector2 xrInputVector2Left;
    private XrInputVector2 xrInputVector2Right;

    void AwakeMovement()
    {
        // Get character controller component
        characterController = GetComponent<CharacterController>();
        // Get objects
        objectXrRig = GameObject.FindWithTag("XrRig");
//        objectCameraFloorOffset = system.xrRig.cameraFloorOffsetObject;
        objectMainCamera = GameObject.FindWithTag("MainCamera");
    }

    // Start is called before the first frame update
    void StartMovement()
    {
        // Make sure events are initialized
        if (eventStateRun    == null) eventStateRun    = new EventStateBool();
        if (eventStateCrouch == null) eventStateCrouch = new EventStateBool();
        if (eventStateFly    == null) eventStateFly    = new EventStateBool();
        // Make sure states are set
        SetStateRun(stateRun);
        SetStateCrouch(stateCrouch);
        SetStateFly(stateFly);
        // Right controller setup
        xrInputVector2Right = new XrInputVector2(controllerRight, CommonUsages.primary2DAxis);
        xrInputBoolJump     = new XrInputBool   (controllerRight, CommonUsages.primaryButton);
        xrInputBoolFly      = new XrInputBool   (controllerRight, CommonUsages.secondaryButton);
        xrInputBoolCrouch   = new XrInputBool   (controllerRight, CommonUsages.primary2DAxisClick);
        // Left controller setup
        xrInputVector2Left  = new XrInputVector2(controllerLeft,  CommonUsages.primary2DAxis);
        xrInputBoolRun      = new XrInputBool   (controllerLeft,  CommonUsages.primary2DAxisClick);
    }

    // Update is called once per frame
    void FixedUpdateMovement()
    {
        // Zero movement and rotation at start of update
        vectorMove = Vector3.zero;
        rotateY = 0.0f;
        // Grounded ?
        if (characterController.isGrounded)
        {
            // Zero speedY
            speedY = 0;
        }
        // In air ?
        else 
        {
            // Not flying and positive Y speed and collided above ?
            if (false == stateFly && speedY > 0.0f && (characterController.collisionFlags & CollisionFlags.Above) != 0)
            {
                // Zero speed y
                speedY = 0;
            }
        }
        // Rotate player to xr rig camera
        RotatePlayerToXrRigCamera();
        // Update movement based upon controller input
        ReadInput();
        // Flying ?
        if (stateFly)
        {
            // Zero speed Y
            speedY = 0;
        }
        // Not flying ? 
        else
        {
            // Apply gravity
            ApplyGravity();
        }
        // Move character controller
        characterController.Move(vectorMove * Time.deltaTime);
        // Position the camera
        MoveXrRigCameraToPlayer();
        // Rotate ?
        if (rotateY != 0.0f)
        {
            // Rotate xr rig (and camera)
            system.xrRig.RotateAroundCameraUsingRigUp(rotateY);
            // Synchronise player rotation
            RotatePlayerToXrRigCamera();
        }
    }

    // Rotate player to xr rig camera (y axis only)
    private void RotatePlayerToXrRigCamera()
    {
        // Rotate player object on the Y axis to match that of the camera
        transform.eulerAngles = new Vector3(0.0f, objectMainCamera.transform.rotation.eulerAngles.y, 0.0f);
    }    

    // Move xr rig camera to player location (plus character controller height)
    private void MoveXrRigCameraToPlayer()
    {
        // Calculate position at top of character controller
        Vector3 positionCamera = transform.position;
        positionCamera.y += characterController.height;
        // Apply to xr rig camera
        system.xrRig.MoveCameraToWorldLocation(positionCamera);
    }

    // Read inputs
    private void ReadInput()
    {
        Vector2 vector2Left;
        Vector2 vector2Right;
        bool    doJump = false;

        // Controllers enabled ?
        if(controllerLeft.enableInputActions && controllerRight.enableInputActions)
        {
            // Run button pressed - try to toggle state
            if (xrInputBoolRun.ReadPressed()) TryToggleRun();
            // Crouch button pressed  - try to toggle state
            if (xrInputBoolCrouch.ReadPressed()) TryToggleCrouch();
            // Fly button pressed - try to toggle state
            if (xrInputBoolFly.ReadPressed()) TryToggleFly();
            // Read vector 2's 
            vector2Left  = xrInputVector2Left.Read();
            vector2Right = xrInputVector2Right.Read();
            // Read jump
            doJump = xrInputBoolJump.ReadPressed();
            // Calculate movements based upon inputs 
            MoveXYZ(vector2Left.x, vector2Right.y, vector2Left.y, doJump);
            // Calculate rotations based upon inputs
            RotateSnapY(vector2Right.x);
        }
    }

    // Try to toggle run
    private void TryToggleRun()
    {
        // Allowed to run ?
        if (canRun)
        {
            // Toggle run
            SetStateRun(!stateRun);
        }
    }

    // Set run state
    private void SetStateRun(bool value)
    {
        // Store new setting
        stateRun = value;
        // Raise event
        if (eventStateRun != null)
        {
            Debug.Log("eventStateRun.Invoke(): " + stateRun, this);
            eventStateRun.Invoke("stateRun", stateRun, (stateRun ? "Running" : "Walking"));
        }
    }

    // Try to toggle crouch
    private void TryToggleCrouch()
    {
        // Allowed to crouch ?
        if (canCrouch)
        {
            // Currently standing ?
            if (false == stateCrouch)
            {
                // Toggle crouch
                SetStateCrouch(!stateCrouch);
            }
            // Currently crouched ?
            else
            {
                RaycastHit hit;

                // Calculate origin for spherecast (head of character controller capsule)
                Vector3 positionOrigin = transform.position;
                positionOrigin.y += characterController.height - characterController.radius;
                // Not going to hit anything ?
                if (false == Physics.SphereCast(positionOrigin, characterController.radius, transform.up, out hit, (heightStand - heightCrouch)))
                {
                    // Toggle crouch
                    SetStateCrouch(!stateCrouch);                    
                }
            }
        }
    }
    
    // Set crouch state
    private void SetStateCrouch(bool value)
    {
        // Store new setting
        stateCrouch = value;
        // Set height
        characterController.height = (stateCrouch ? heightCrouch : heightStand);
        // Adjust center
        Vector3 newCenter = characterController.center;
        newCenter.y = characterController.height / 2;
        newCenter.y += characterController.skinWidth;
        characterController.center = newCenter;
        // Raise event
        if (eventStateCrouch != null)
        {
            Debug.Log("eventStateCrouch.Invoke(): " + stateCrouch, this);
            eventStateCrouch.Invoke("stateCrouch", stateCrouch, (stateCrouch ? "Crouching" : "Standing"));
        }
    } 

    // Try to toggle fly
    private void TryToggleFly()
    {
        // Allowed to fly ?
        if (canFly)
        {
            // Toggle fly
            SetStateFly(!stateFly);
        }
    }

    private void SetStateFly(bool value)
    {
        // Store new setting
        stateFly = value;
        // Raise event
        if (eventStateFly != null)
        {
            Debug.Log("eventFly.Invoke(): " + stateFly, this);
            eventStateFly.Invoke("stateFly", stateFly, (stateFly ? "Flying" : "Grounded"));
        }
    }

    // Move along XYZ axes
    private void MoveXYZ(float moveX, float moveY, float moveZ, bool moveJump)
    {
        // Apply the XZ to the player's forward direction
        Vector3 direction = new Vector3(moveX, 0, moveZ);
        Vector3 playerRotation = new Vector3(0, transform.eulerAngles.y, 0);
        // Rotate the input direction by the horizontal head rotation
        direction = Quaternion.Euler(playerRotation) * direction;
        // Running ?
        if (stateRun)
        {
            // Apply run
            vectorMove = direction * speedRun;
        }
        // Walking ?
        else
        {
            // Apply walk
            vectorMove = direction * speedWalk;
        }
        
        // Flying ? 
        if (stateFly)
        {
            // Running ?
            if (stateRun)
            {
                // Apply run
                vectorMove.y = moveY * speedRun;
            }
            // Walking ?
            else
            {
                // Apply walk
                vectorMove.y = moveY * speedWalk;
            }
        }
        // Not flying ?
        else
        {
            // Jump if allowed ? 
            if (moveJump  && canJump && characterController.isGrounded)
            {
                speedY += speedJump;
            }
        }
    }

    // Rotate around Y axis
    private void RotateSnapY(float inputY)
    {
        int state = 0;

        // Update timer
        if (snapTimer > 0.0f) snapTimer -= Time.deltaTime;
        // Figure out new turn state
        if (inputY >= snapThreshold) state = 1;
        else if (inputY <= -snapThreshold) state = -1;
        // Turn state changed ? 
        if (snapState != state)
        {
            // Note new state
            snapState = state;
            // Returned to idle position ?
            if (snapState == 0)
            {
                // Timer not running - start it
                if (snapTimer <= 0.0f) snapTimer = snapTimeout;
            }
            // Want to turn and allowed ?
            else if (snapTimer <= 0.0f)
            {
                // Set rotation
                rotateY = snapState * snapDegrees;
            }
        }
    }

    private void ApplyGravity()
    {
        // Update speedY for gravity
        speedY += Physics.gravity.y * Time.deltaTime;
        // Include speedY in move vector
        vectorMove.y = speedY;
    }
}
