﻿// Unity XR Input Handler
//
// Setup:   https://www.youtube.com/watch?v=ndwJHpxd9Mo
// Inputs:  https://docs.unity3d.com/ScriptReference/XR.CommonUsages.html
//
// ==============================================================================
//
// MIT License
//
// Copyright(c) 2020 Martin Looker
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

// Handler for boolean inputs, the following InputFeatureUsages are available:
// CommonUsages.triggerButton       
// CommonUsages.gripButton
// CommonUsages.primaryButton
// CommonUsages.secondaryButton
// CommonUsages.menuButton
// CommonUsages.primary2DAxisClick
// CommonUsages.secondary2dAxisClick
// CommonUsages.primaryTouch
// CommonUsages.secondaryTouch
// CommonUsages.primary2DAxisTouch
// CommonUsages.secondary2dAxisTouch
// Only one of the read functions should be used, use the one you are interested in monitoring
public class XrInputBool 
{
    public XRController xrController;
    public InputFeatureUsage<bool> inputFeatureUsage;
    public bool state = false;

    public XrInputBool(
        XRController            newXrController,            // XR Controller to monitor
        InputFeatureUsage<bool> newInputFeatureUsage)       // Boolean input feature to monitor
    {
        xrController = newXrController;
        inputFeatureUsage = newInputFeatureUsage;
    }

    // Returns the current state of the button (no change monitoring)
    public bool ReadState()
    {
        bool read = false;

        // Valid setup ?
        if (xrController != null)
        {
            // Can we access the feature on the controller ? 
            if (xrController.inputDevice.TryGetFeatureValue(inputFeatureUsage, out bool value))
            {
                // Note and return value 
                state = read = value;
            }
        } 

        return read;       
    }

    // Returns whether the input has changed (pressed or released)
    public bool ReadChanged()
    {
        bool read = false;

        // Valid setup ?
        if (xrController != null)
        {
            // Can we access the feature on the controller ? 
            if (xrController.inputDevice.TryGetFeatureValue(inputFeatureUsage, out bool value))
            {
                // Button changed ?
                if (state != value)
                {
                    // Note new value 
                    state = value;
                    // Return true
                    read = true;
                }
            }
        } 

        return read;       
    }    

    // Returns whether the input has been pressed
    public bool ReadPressed()
    {
        bool read = false;

        // Valid setup ?
        if (xrController != null)
        {
            // Can we access the feature on the controller ? 
            if (xrController.inputDevice.TryGetFeatureValue(inputFeatureUsage, out bool value))
            {
                // Button changed ?
                if (state != value)
                {
                    // Note new value 
                    state = value;
                    // Transition we are looking for ?
                    if (state == true)
                    {
                        // Return true
                        read = true;
                    }
                }
            }
        } 

        return read;       
    }

    // Return whether the input has been released
    public bool ReadReleased()
    {
        bool read = false;

        // Valid setup ?
        if (xrController != null)
        {
            // Can we access the feature on the controller ? 
            if (xrController.inputDevice.TryGetFeatureValue(inputFeatureUsage, out bool value))
            {
                // Button changed ?
                if (state != value)
                {
                    // Note new value 
                    state = value;
                    // Transition we are looking for ?
                    if (state == false)
                    {
                        // Return true
                        read = true;
                    }
                }
            }
        } 

        return read;       
    }    
}

// Handler for float inputs, the following InputFeatureUsages are available:
// CommonUsages.grip
// CommonUsages.trigger
public class XrInputFloat 
{
    public XRController xrController;
    public InputFeatureUsage<float> inputFeatureUsage;
    public float state = 0.0f;

    public XrInputFloat(
        XRController             newXrController,       // XR Controller to monitor
        InputFeatureUsage<float> newInputFeatureUsage)       // Initial state
    {
        xrController = newXrController;
        inputFeatureUsage = newInputFeatureUsage;
    }

    public float Read()
    {
        // Valid setup ?
        if (xrController != null)
        {
            // Can we access the feature on the controller ? 
            if (xrController.inputDevice.TryGetFeatureValue(inputFeatureUsage, out float value))
            {
                // Note new value 
                state = value;
            }
        } 

        return state;       
    }
}

// Handler for Vector2 inputs, the following InputFeatureUsages are available:
// CommonUsages.primary2DAxis
// CommonUsages.secondary2DAxis
public class XrInputVector2 
{
    public XRController xrController;
    public InputFeatureUsage<Vector2> inputFeatureUsage;
    public Vector2 state = new Vector2(0.0f, 0.0f);

    public XrInputVector2 (
        XRController               newXrController,       // XR Controller to monitor
        InputFeatureUsage<Vector2> newInputFeatureUsage)       // Initial state
    {
        xrController = newXrController;
        inputFeatureUsage = newInputFeatureUsage;
    }

    public Vector2 Read()
    {
        // Valid setup ?
        if (xrController != null)
        {
            // Can we access the feature on the controller ? 
            if (xrController.inputDevice.TryGetFeatureValue(inputFeatureUsage, out Vector2 value))
            {
                // Note new value 
                state = value;
            }
        } 

        return state;       
    }
}

// Handler for Vector3 inputs, for positions, rotations and accelerations
public class XrInputVector3 
{
    public XRController xrController;
    public InputFeatureUsage<Vector3> inputFeatureUsage;
    public Vector3 state = new Vector3(0.0f, 0.0f, 0.0f);

    public XrInputVector3 (
        XRController               newXrController,       // XR Controller to monitor
        InputFeatureUsage<Vector3> newInputFeatureUsage)       // Initial state
    {
        xrController = newXrController;
        inputFeatureUsage = newInputFeatureUsage;
    }

    public Vector3 Read()
    {
        // Valid setup ?
        if (xrController != null)
        {
            // Can we access the feature on the controller ? 
            if (xrController.inputDevice.TryGetFeatureValue(inputFeatureUsage, out Vector3 value))
            {
                // Note new value 
                state = value;
            }
        } 

        return state;       
    }
}