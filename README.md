# marjohloo: unity-xr-first-person #

## Overview ##

This first person controller for the Unity XR Interaction Toolkit provides the following features:

* XZ movement
* Y Axis snap turn
* Jumping (with immediate drop if colliding into aan object above), can be disabled
* Walk/Run, can be disabled
* Crouch/Uncrouch, can be disabled
* Grounded/Fly (movement on Y axis), can be disabled

## Controls ##

### Left Controller ###

**Primary 2D Axis:** Movement on XZ plane

**Primary 2D Axis Click:** Walk/Run toggle (if enabled)

### Right Controller ###

**Primary 2D Axis (Horizontal):** Snap turn

**Primary 2D Axis (Vertical):** Up/Down on Y axis (when in fly mode)

**Primary 2D Axis Click:** Stand/Crouch toggle (if enabled)

**Primary Button:** Jump (if enabled)

**Secondary Button:** Grounded/Fly toggle (if enabled)

## Implementation ##

The player game object (with character controller) and XR Rig are separate, the controller script is attached to the player object (not the XR Rig).

The first person controller is split using partial classes. `XrFirstPerson.cs` is the wrapper class but is largely empty. `XrFirstPersonMovement.cs` is where the real work is done to perform the movement. The reason for this split is that I plan to add further modules for other functionality (such as interactions) with the intention that it will be easy to swap the modules in and out for different functionality.

The basic steps are:

1. Rotate the player object to match the rotation of the XR Rig camera
2. Read the inputs and collect movements and rotation inputs as required
3. Move the character controller on the player object
4. Move the XR Rig to where the character controller is
5. Rotate the XR Rig around the camera if snap turning and resynchronise the rotation of the playuer object

### Lessons Learned ###

Be careful not to attach objects with mesh colliders to the XR Rig, they will collide with the character controller and totally mess up movement direction!

## Versions ##

Built on Unity 2019.3.14f1 using Oculus Rift.

## Packages ##

Some of these are previews, to access them from *Package Manager* you may need to use *Advanced > Show preview packages*.

**[ProBuilder (4.2.3)](https://docs.unity3d.com/Packages/com.unity.probuilder@4.0/manual/index.html)**

**[ProGrids (preview.6 - 3.0.3)](https://docs.unity3d.com/Packages/com.unity.progrids@3.0/manual/index.html)**

**[XR Interaction Toolkit (preview - 0.9.4)](https://blogs.unity3d.com/2019/12/17/xr-interaction-toolkit-preview-package-is-here/)**

**[XR Plugin Management (3.2.10)](https://docs.unity3d.com/Packages/com.unity.xr.management@3.2/manual/index.html)** 

## References ##

**[XR Interaction Toolkit (YouTube - VR with Andrew)](https://youtu.be/ndwJHpxd9Mo)** - basic setup.

**[XR Toolkit Movement (YouTube - VR with Andrew)](https://www.youtube.com/playlist?list=PLmc6GPFDyfw87eECUxsoysoIz82VifRwt)** - basic movement but really for roomscale implementations 
with the character controller height following the position of the headset. The implementation in this code is for a stationary first person view with the in-game height locked regardless
of the headset position.

**[Gravity with character controller (Unity Answers - aldonaletto)](https://answers.unity.com/questions/334708/gravity-with-character-controller.html)** - how to apply gravity to a 
character controller.

**[Create a Crouching System (YouTube - DitzelGames)](https://www.youtube.com/watch?v=Wepj2-K5l_g)** - code here explains how to check if there is room above to uncrouch, adapted
in this implementation to use a spherecast (instead of a raycast).

**[Character Floats After Hitting Head on Object (Unity Answers - ddmx)](https://answers.unity.com/questions/404599/character-floats-after-hitting-head-on-object.html)** - code here
to detect collision above character controller used in this implementation to immediately drop when jumping up and hitting something above.

## Disclaimers ##

Whilst I'm an embedded C programmer for my day job I've only been learning Unity for a few weeks and XR for an even shorter time. I'm just watching and reading stuff on the web to figure things out, so these implementations might be wildly inappropriate. 

## License ##

This code is released under the MIT License, see [LICENSE.txt](LICENSE.txt) for details.